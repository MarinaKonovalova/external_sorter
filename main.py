import external_sorter

if __name__ == "__main__":
   external_sorter.ExternalSorter().sort('input.txt',
                          'output.txt',
                          lambda x, y: -1 if x < y else 1)
