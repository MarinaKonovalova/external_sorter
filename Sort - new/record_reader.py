class RecordReader:
    def __init__(self, file_name):
        self.file_name = file_name

    def __enter__(self):
        self.file = open(self.file_name)
        return self

    def __iter__(self):
        return self

    def __next__(self):
        line = self.file.readline()
        if not line:
            raise StopIteration
        return line

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.file.close()
