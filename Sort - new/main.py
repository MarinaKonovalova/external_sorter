import external_sorter

def comparator (items):
   return items.index(min(items))

if __name__ == "__main__":   
   external_sorter.ExternalSorter().sort('input.txt',
                          'output.txt',
                          comparator)
