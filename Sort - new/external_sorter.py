import tempfile
import os
import uuid
import record_reader
import record_writer


def temporary_file_name():
    return os.path.join(tempfile.gettempdir(), str(uuid.uuid1()))


def read_or_none(reader):
    try:
        return reader.__next__()
    except StopIteration:
        return None


class ExternalSorter:
    def __init__(self):
        self.fix_tempfile_size = 10
    
    def sort(self,
             input_file_path,
             output_file_path,
             comparator):
        count_files = self.count_items(input_file_path) // self.fix_tempfile_size
        if count_files == 0:
            count_files = 1
        mod = self.count_items(input_file_path) - (self.fix_tempfile_size * (count_files - 1))
        self._sort_internal(self, count_files, mod, input_file_path, output_file_path, comparator)

    @staticmethod
    def count_items(file_path):
        count = 0
        with record_reader.RecordReader(file_path) as reader:
            for _ in reader:
                count = count + 1
        return count

    

    @staticmethod
    def _sort_internal(self, count_files, mod,
                       input_file_path,
                       output_file_path,
                       comparator):
        
        temp_files = []
        sorted_file_names = []
        items = []
        files = []
        
        if self.count_items(input_file_path) < 2:
            with record_reader.RecordReader(input_file_path) as reader:
                with record_writer.RecordWriter(output_file_path) as writer:
                    for item in reader:
                        writer.write(item)
        else:
            for _ in range(count_files):
                temp_files.append(temporary_file_name())

            with record_reader.RecordReader(input_file_path) as reader:
               for file_name in temp_files:
                    with record_writer.RecordWriter(file_name) as writer:
                        for _ in range(self.fix_tempfile_size):
                            if read_or_none(reader) is not None:
                                writer.write(reader.__next__()) # тут ошибка!
                            else:
                                break

            for _ in range(count_files):
                sorted_file_names.append(temporary_file_name())

            for i in range(count_files):
                ExternalSorter._sort_internal(self, self.fix_tempfile_size,
                                          temp_files[i-1],
                                          sorted_file_names[i-1],
                                          comparator)
            
            with record_writer.RecordWriter(output_file_path) as writer:
                for sorted_file_name in sorted_file_names:
                    with record_reader.RecordReader(sorted_file_name) as file:
                        files.append(file)
                        items.append(read_or_none(file))
                while len(items) > 0: #items.count(None) < count_files and 
                    if items.count(None) == 0:
                        number_file_min_item = comparator(items)
                        writer.write(items[number_file_min_item])
                        items[number_file_min_item] = read_or_none(files[number_file_min_item])
                    else:
                        count = -1
                        for item in items:
                            count = count + 1
                            if item is None:
                                items.pop([count])
            

'''
            with record_reader.RecordReader(sorted_first_file_name) as first_file:
                with record_reader.RecordReader(sorted_second_file_name) as second_file:
                    with record_writer.RecordWriter(output_file_path) as writer:
                        first_item = read_or_none(first_file)
                        second_item = read_or_none(second_file)
                        while (first_item is not None) or (second_item is not None):
                            if (first_item is not None) and (second_item is not None):
                                if comparator(first_item, second_item) <= 0:
                                    writer.write(first_item)
                                    first_item = read_or_none(first_file)
                                else:
                                    writer.write(second_item)
                                    second_item = read_or_none(second_file)
                            else:
                                if first_item is not None:
                                    writer.write(first_item)
                                    first_item = read_or_none(first_file)
                                if second_item is not None:
                                    writer.write(second_item)
                                    second_item = read_or_none(second_file)
'''
