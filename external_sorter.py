import tempfile
import os
import uuid
import record_reader
import record_writer


def temporary_file_name():
    return os.path.join(tempfile.gettempdir(), str(uuid.uuid1()))


def read_or_none(reader):
    try:
        return reader.__next__()
    except StopIteration:
        return None


class ExternalSorter:
    def sort(self,
             input_file_path,
             output_file_path,
             comparator):
        n = self._count_items(input_file_path)
        self._sort_internal(n, input_file_path, output_file_path, comparator)

    @staticmethod
    def _count_items(file_path):
        count = 0
        with record_reader.RecordReader(file_path) as reader:
            for _ in reader:
                count = count + 1
        return count

    @staticmethod
    def _sort_internal(n,
                       input_file_path,
                       output_file_path,
                       comparator):
        if n < 2:
            with record_reader.RecordReader(input_file_path) as reader:
                with record_writer.RecordWriter(output_file_path) as writer:
                    for item in reader:
                        writer.write(item)
        else:
            first_file_size = n // 2

            first_file_name = temporary_file_name()
            second_file_name = temporary_file_name()

            with record_reader.RecordReader(input_file_path) as reader:
                with record_writer.RecordWriter(first_file_name) as writer:
                    for _ in range(first_file_size):
                        writer.write(reader.__next__())
                with record_writer. RecordWriter(second_file_name) as writer:
                    for _ in range(n - first_file_size):
                        writer.write(reader.__next__())

            sorted_first_file_name = temporary_file_name()
            sorted_second_file_name = temporary_file_name()

            ExternalSorter._sort_internal(first_file_size,
                                          first_file_name,
                                          sorted_first_file_name,
                                          comparator)

            ExternalSorter._sort_internal(n - first_file_size,
                                          second_file_name,
                                          sorted_second_file_name,
                                          comparator)

            with record_reader.RecordReader(sorted_first_file_name) as first_file:
                with record_reader.RecordReader(sorted_second_file_name) as second_file:
                    with record_writer. RecordWriter(output_file_path) as writer:
                        first_item = read_or_none(first_file)
                        second_item = read_or_none(second_file)
                        while (first_item is not None) or (second_item is not None):
                            if (first_item is not None) and (second_item is not None):
                                if comparator(first_item, second_item) <= 0:
                                    writer.write(first_item)
                                    first_item = read_or_none(first_file)
                                else:
                                    writer.write(second_item)
                                    second_item = read_or_none(second_file)
                            else:
                                if first_item is not None:
                                    writer.write(first_item)
                                    first_item = read_or_none(first_file)
                                if second_item is not None:
                                    writer.write(second_item)
                                    second_item = read_or_none(second_file)
