class RecordWriter:
    def __init__(self, file_name):
        self.file_name = file_name

    def __enter__(self):
        self.file = open(self.file_name, 'w')
        return self

    def write(self, item):
        self.file.write(item)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.file.close()
